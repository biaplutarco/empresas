//
//  SearchHeaderViewDelegate.swift
//  Empresas
//
//  Created by Bia on 19/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

protocol SearchHeaderViewDelegate: class {
    func filterDataForSearchText(_ searchText: String)
}
