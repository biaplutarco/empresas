//
//  EnterpriseCellViewModel.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class EnterpriseCellViewModel: ImageViewModel {
    
    var name: String?
        
    init(withImagePath imagePath: String?, andName name: String?) {
        
        self.name = name
        
        super.init(imagePath: imagePath)
    }
}
