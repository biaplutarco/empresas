//
//  PhotoView.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class PhotoView: UIView {
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel(titleColor: .white)
        
        return label
    }()
    
    init(withBackgroundColor color: UIColor, cornerRadius: CGFloat, andTitleSize size: CGFloat) {
        super.init(frame: .zero)
        
        imageView.backgroundColor = color
        imageView.layer.cornerRadius = cornerRadius
        nameLabel.font = UIFont.boldSystemFont(ofSize: size)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData(forImage image: UIImage?) {

        DispatchQueue.main.async {
            self.imageView.image = image
        }
    }
    
    func reloadData(forName name: String?) {
        self.nameLabel.text = name
    }
}

extension PhotoView: ViewCodable {
    
    func buildViewHierarchy() {
        
        addSubview(imageView)
        addSubview(nameLabel)
    }
    
    func setupConstraints() {
        
        nameLabel.centeredToSuperview()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}

