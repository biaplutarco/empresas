//
//  EnterpriseCell.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    
    var viewModel: EnterpriseCellViewModel? {
        didSet {
            reloadData()
        }
    }

    private lazy var photoView: PhotoView = {
        let photoView = PhotoView(withBackgroundColor: .action, cornerRadius: 4, andTitleSize: 20)
        
        return photoView
    }()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        selectionStyle = .none
    }
    
    private func reloadData() {
        
        photoView.reloadData(forName: viewModel?.name)
        
        viewModel?.reloadImage = {
            
            self.photoView.reloadData(forImage: self.viewModel?.image)
        }
    }
}

extension EnterpriseCell: ViewCodable {
    
    func buildViewHierarchy() {
        addSubview(photoView)
    }
    
    func setupConstraints() {
        photoView.fulfillSuperview()
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}
