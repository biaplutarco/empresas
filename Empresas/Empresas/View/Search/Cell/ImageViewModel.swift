//
//  ImageViewModel.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class ImageViewModel: ImageDownloadable {
    
    private var imageURL: String?
    
    var image: UIImage? {
        didSet {
            reloadImage?()
        }
    }
    
    var reloadImage: (() -> Void)?
    
    init(imagePath: String?) {
        self.imageURL = imagePath
        
        setImage()
    }
    
    func setImage() {
        
        downloadImage(withPath: imageURL) { image, error in
            
            if let image = image {
                
                self.image = image
            } else {
            
                print(error?.localizedDescription as Any)
            }
        }
    }
}
