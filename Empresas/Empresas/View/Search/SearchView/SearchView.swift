//
//  SearchView.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

protocol SearchViewDelegate: class {
    func present(_ viewcontroller: DetailsViewController)
}

class SearchView: UIView {
    
    weak var delegate: SearchViewDelegate?
    
    private var viewModel: SearchViewModel
        
    private lazy var headerView: SearchHeaderView = {
        let headerView = SearchHeaderView()
        headerView.delegate = self
        
        return headerView
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(withSeparatorStyle: .none, backgroundColor: .clear)
        tableView.registerCell(of: EnterpriseCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        return tableView
    }()
    
    init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        
        super.init(frame: CGRect.zero)
        
        setupView()
        reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        
        viewModel.reloadData = {
           
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

extension SearchView: ViewCodable {
    
    func buildViewHierarchy() {
        
        addSubview(tableView)
        addSubview(headerView)
    }
    
    func setupConstraints() {
        
        headerView.headeredToSuperview()
        tableView.belowTo(view: headerView)
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}

extension SearchView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueCell(of: EnterpriseCell.self, forIndexPath: indexPath)
        cell.setupCell(withViewModel: viewModel.getEnterpriseCellViewModel(for: indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.present(DetailsViewController(viewModel: viewModel.getEnterpriseDetailsViewModel(for: indexPath.row)))
    }
}

extension SearchView: SearchHeaderViewDelegate {
    
    func filterDataForSearchText(_ searchText: String) {
        viewModel.search(forName: searchText)
    }
}

extension SearchView: Keyboardable {
    
    @objc func keyboardWillShow(_ notification: Notification) {
        tableView.adjustInsetForKeyboardShow(true, notification: notification)
    }
       
    @objc func keyboardWillHide(_ notification: Notification) {
        tableView.adjustInsetForKeyboardShow(false, notification: notification)
    }
}
