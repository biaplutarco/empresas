//
//  SearchViewModel.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class SearchViewModel: NSObject, Searchable {
    
    var isFiltering: Bool = false {
        didSet {
            setData()
        }
    }
    
    var searchText: String = "" {
        didSet {
            if searchText == "" {
                isFiltering = false
            }
        }
    }
        
    var data: [Enterprise] = [] {
        didSet {
            reloadData?()
        }
    }
    
    var reloadData: (() -> Void)? {
        didSet {
            setData()
        }
    }

    override init() {
        super.init()
        
        setData()
    }
    
    private func setData() {
        
        if data.isEmpty {
            
            setEnterprises()
        } else {
            
            if isFiltering {
                
                getFilteredEnterprises()
                
            }
        }
    }
    
    private func setEnterprises() {
        
        allEnterprises { enterprises, error in
            
            if let enterprises = enterprises {
                
                self.data = enterprises
            } else {
                
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    private func getFilteredEnterprises() {
        
        data = data.filter { (enterprise: Enterprise) -> Bool in
            
            guard let name = enterprise.name else { return false }
            
            return didContaints(searchText, onTitle: name)
        }
        
        reloadData?()
    }
    
    func didContaints(_ searchText: String, onTitle title: String) -> Bool {
        return title.lowercased().contains(searchText.lowercased())
    }
    
    func getEnterpriseCellViewModel(for row: Int) -> EnterpriseCellViewModel {
        
        let viewModel = EnterpriseCellViewModel(withImagePath: data[row].photo, andName: data[row].name)

        return viewModel
    }
    
    func getEnterpriseDetailsViewModel(for row: Int) -> DetailsViewModel {
        
        let viewModel = DetailsViewModel(withImagePath:data[row].photo, name: data[row].name, description: data[row].description)

        return viewModel
    }
    
    func search(forName name: String) {
        
        isFiltering = true
        searchText = name
    }
}
