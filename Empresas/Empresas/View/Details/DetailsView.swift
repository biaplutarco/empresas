//
//  DetailsView.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class DetailsView: UIView {
    
   var viewModel: DetailsViewModel? {
        didSet {
            reloadData()
        }
    }

    private lazy var photoView: PhotoView = {
        let imageView = PhotoView(withBackgroundColor: .top, cornerRadius: 0, andTitleSize: 36)
        
        return imageView
    }()
    
    private lazy var descriptionView: UITextView = {
        let textView = UITextView()
        textView.textColor = .labelText
        textView.isUserInteractionEnabled = false
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        
        return textView
    }()
    
    init() {
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func reloadData() {
        
        descriptionView.text = viewModel?.description
        
        photoView.reloadData(forName: viewModel?.name)
        
        viewModel?.reloadImage = {
            
            self.photoView.reloadData(forImage: self.viewModel?.image)
        }
    }
}

extension DetailsView: ViewCodable {
    
    func buildViewHierarchy() {
        
        addSubview(photoView)
        addSubview(descriptionView)
    }
    
    func setupConstraints() {
        
        photoView.headeredToSuperview()
        descriptionView.belowTo(view: photoView)
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}
