//
//  EnterpriseViewModel.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class DetailsViewModel: ImageViewModel {
    
    var name: String?
    var description: String?
    
    init(withImagePath imagePath: String?, name: String?, description: String?) {
        self.name = name
        self.description = description
        
        super.init(imagePath: imagePath)
    }
}
