//
//  LoginView.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class LoginView: UIView, Loginable {
            
    private lazy var headerView: LogoHeaderView = {
        let headerView = LogoHeaderView()
        
        return headerView
    }()
    
    private lazy var inpuView: CredentialsInputView = {
        let inpuView = CredentialsInputView()
        
        return inpuView
    }()
    
    private lazy var signinButton: LoadingButton = {
        let button = LoadingButton(withColor: .action, titleColor: .white,
                              cornerRadius: 8, andTitle: .siginin)
        button.addTarget(self, action: #selector(doSignin), for: .touchUpInside)
        
        return button
    }()
    
    init(withSemiCircleOf size: CGSize) {
        super.init(frame: CGRect.zero)
        
        setupView()
        
        applySemiCircle(withSize: size)
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(loginError),
                         name: NSNotification.Name("LoginError"), object: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        
        NotificationCenter
            .default
            .removeObserver(self, name: NSNotification.Name("LoginError"), object: nil)
    }
    
    func applySemiCircle(withSize size: CGSize) {
        headerView.applySemiCircle(withSize: size)
    }
    
    @objc func loginError() {
        signinButton.stopLoading()
    }
    
    @objc func doSignin() {
            
        let userCredentials = inpuView.getCredentials()
        
        signinButton.startLoading()
        doLogin(withUserCredentials: userCredentials)
    }
}

extension LoginView: ViewCodable {
    
    func buildViewHierarchy() {
        
        addSubview(headerView)
        addSubview(inpuView)
        addSubview(signinButton)
    }
    
    func setupConstraints() {
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.widthAnchor.constraint(equalTo: widthAnchor),
            headerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3),
            
            inpuView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 30),
            inpuView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            inpuView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            inpuView.bottomAnchor.constraint(equalTo: signinButton.topAnchor, constant: -40),
            
            signinButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            signinButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            signinButton.heightAnchor.constraint(equalToConstant: 48),
        ])
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}
