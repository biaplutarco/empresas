//
//  LogoView.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class LogoView: UIView {
    
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: .logoIcon)
        
        return imageView
    }()
    
    private lazy var label: UILabel = {
        let imageView = UILabel(titleColor: .white, andTitle: Text.welcome.rawValue)
        
        return imageView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [logoImageView, label])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 6
        
        return stackView
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LogoView: ViewCodable {
    
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.fulfillSuperview()
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}
