//
//  LogoView.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class LogoHeaderView: UIView {
        
    private lazy var gradientView: UIView = {
        let view = UIView()
        view.backgroundColor = .action
            
        return view
    }()
    
    private lazy var logoView: LogoView = {
        let logoView = LogoView()
        
        return logoView
    }()
    
    init() {
        
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applySemiCircle(withSize size: CGSize) {
        gradientView.applyGradient(colors: [.top, .bottom], direction: .rightToLeft, size: size)
        gradientView.doSemiCircle(withSize: size)
    }
}

extension LogoHeaderView: ViewCodable {
    
    func buildViewHierarchy() {
        
        addSubview(gradientView)
        addSubview(logoView)
    }
    
    func setupConstraints() {
        gradientView.fulfillSuperview()
        logoView.centeredToSuperview()
    }
    
    func setupAdditionalConfiguration() {
        
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }
}
