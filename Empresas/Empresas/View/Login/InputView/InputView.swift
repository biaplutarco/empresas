//
//  InputView.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class InputView: UIView {
        
    var type: InputType
    
    private lazy var visibilityButton: UIButton = {
        let button = UIButton(withImage: .visibility)
        button.addTarget(self, action: #selector(passawordVisibility), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton(withImage: .cancel)
        button.addTarget(self, action: #selector(clean), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(titleColor: .labelText, andTitle: type.rawValue)
        
        return label
    }()
    
    private lazy var textField: UITextField = {
        let textField = UITextField(withCornerRadius: 4, color: (.action, .background),
                                    type: type, andRigthButton: visibilityButton)
        textField.delegate = self
        
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel(titleColor: .error, andTitle: Text.credentialsError.rawValue)
        label.textAlignment = .right
        
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, textField])
        stackView.axis = .vertical
        stackView.spacing = 4
        
        return stackView
    }()
        
    init(type: InputType) {
        
        self.type = type
        
        super.init(frame: CGRect.zero)
        
        setupView()
        addObservers()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        removeObservers()
    }
    
    private func addObservers() {
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(activeErrorMode),
                         name: NSNotification.Name("LoginError"), object: nil)
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(hideKeyboard(_:)),
                     name: NSNotification.Name("HideKeyboard"), object: nil)
    }
    
    func removeObservers() {
        
        NotificationCenter
            .default
            .removeObserver(self, name: NSNotification.Name("LoginError"), object: nil)
        
        NotificationCenter
            .default
            .removeObserver(self, name: NSNotification.Name("HideKeyboard"), object: nil)
    }
    
    func getText() -> String {
        return textField.text ?? ""
    }
    
    @objc func passawordVisibility() {
        textField.isSecureTextEntry.toggle()
    }
    
    @objc func clean() {
        textField.text = ""
    }
    
    @objc func activeErrorMode() {
        
        textField.errorMode(withRightButton: cancelButton)
        
        if type == .password {
            
            stackView.addArrangedSubview(errorLabel)
        }
    }
    
    @objc func hideKeyboard(_ sender: AnyObject) {
        textField.endEditing(true)
    }
}

extension InputView: ViewCodable {
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.fulfillSuperview()
        
        NSLayoutConstraint.activate([
            textField.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
    
    func setupAdditionalConfiguration() {
        backgroundColor = .clear
    }
}

extension InputView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        titleLabel.textColor = .action
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        titleLabel.textColor = .labelText
    }
}
