//
//  LoginInputView.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class CredentialsInputView: UIView {
        
    private lazy var emailView: InputView = {
        let inputView = InputView(type: .email)
        
        return inputView
    }()
    
    private lazy var passwordView: InputView = {
        let inputView = InputView(type: .password)
        
        return inputView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [emailView, passwordView])
        stackView.axis = .vertical
        stackView.spacing = 16
        
        return stackView
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getCredentials() -> [String : String] {
        
        let email = emailView.getText()
        let password = passwordView.getText()
        
        return ["email" : email, "password" : password]
    }
}

extension CredentialsInputView: ViewCodable {
    
    func buildViewHierarchy() {
        addSubview(stackView)
    }
    
    func setupConstraints() {
        stackView.fulfillSuperview()
    }
    
    func setupAdditionalConfiguration() {
        
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }
}
