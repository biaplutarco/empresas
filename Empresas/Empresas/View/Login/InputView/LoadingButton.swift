//
//  LoginButton.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class LoadingButton: UIButton {
    
    private var animator: UIViewPropertyAnimator?
    private var originalTitleColor: UIColor?
    
    var originalButtonTitle: String? {
        didSet {
            titleLabel?.numberOfLines = 0
            titleLabel?.textAlignment = .center
        }
    }
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .lightGray
        
        return activityIndicator
    }()
    
    func startLoading() {
        
        DispatchQueue.main.async {
            
            self.originalButtonTitle = self.titleLabel?.text
            self.originalTitleColor = self.titleLabel?.textColor
            
            self.setTitle(" ", for: .normal)
            
            self.setupActivityIndicatorConstraints()
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopLoading() {
        
        DispatchQueue.main.async {
            
            self.setTitle(self.originalButtonTitle, for: .normal)
            self.activityIndicator.stopAnimating()
        }
    }

    fileprivate func setupActivityIndicatorConstraints() {
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
