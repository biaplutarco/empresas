//
//  SceneDelegate.swift
//  Empresas
//
//  Created by Bia on 13/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let viewController = SearchViewController()

        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        
        if User.current?.id == nil {
            
            let loginViewController = LoginViewController()
            loginViewController.modalPresentationStyle = .fullScreen

            viewController.present(loginViewController, animated: true, completion: nil)
        }
        
        self.window = window
    }
}
