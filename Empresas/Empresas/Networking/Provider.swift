//
//  Provider.swift
//  Empresas
//
//  Created by Bia on 16/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit
import Moya
import PromiseKit

protocol JSONSerializable {
    init(with json: [String : Any])
}

class BackpackerProvider<O: Codable, T: TargetType> {

    typealias Output = O
    typealias TargetAPI = T

    private var decoder = JSONDecoder()
    private var provider = MoyaProvider<TargetAPI>()
    
    init() {
        
        self.decoder.dateDecodingStrategy = .customISO8601
    }
    
    func request(_ target: TargetAPI) -> Promise<Output> {
        return Promise<Output> { seal in
            self.provider.request(target, completion: { (response) in
                
                switch response {
                
                case .success(let result):
                    do {
                        
                        guard 200...299 ~= result.statusCode else {
                            
                            let error = try self.decoder.decode(ResponseError.self, from: result.data)
                            return seal.reject(error)
                        }
                        
                        if Output.self == ResponseSuccess.self {
                            self.savedHeader(result)
                        }
                        
                        let object = try self.decoder.decode(Output.self, from: result.data)
                        
                        return seal.fulfill(object)

                    } catch {
                        
                        return seal.reject(error)
                    }

                case .failure(let error):
                    return seal.reject(error)
                }
            })
        }
    }
    
    private func requestHeader(response: Response, header: String) -> String {
        
        if let header = response.response?.allHeaderFields[header] as? String {
            return header
        }
       
        return ""
    }
    
    private func savedHeader(_ response: Response) {
        let accessToken = self.requestHeader(response: response, header: "access-token")
        let client = self.requestHeader(response: response, header: "client")
        let uid = self.requestHeader(response: response, header: "uid")
        
        User.save(email: uid, id: client, token: accessToken)
    }
}
