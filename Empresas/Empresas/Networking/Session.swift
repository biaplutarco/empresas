//
//  Session.swift
//  Empresas
//
//  Created by Bia on 16/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation
import Moya

enum EnterpriseFetchOptions {
    case all
    case by(name: String)
}

enum SessionAPI {
    case login(bodyParameters: [String : Any])
    case enterprise(EnterpriseFetchOptions)
}

extension SessionAPI: TargetType {
    
    var baseURL: URL {
        
        return URL(string: "https://empresas.ioasys.com.br/api/v1")!
    }
    
    var path: String {
        
        switch self {
            
        case .login:
            
            return "/users/auth/sign_in"
            
        case .enterprise(let fetch):
            
            switch  fetch{
            
            case .all:
                
                return "/enterprises/"
                
            case .by(let name):
                
                return "/enterprises?enterprise_types=1&name=\(name)"
            }
        }
    }
    
    var method: Moya.Method {
        
        switch self {
            
        case .login:
            
            return .post
        
        case .enterprise:
            
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        switch self {
            
        case .login(let bodyParameters):
            
            return .requestCompositeParameters(bodyParameters: bodyParameters,
                                               bodyEncoding: JSONEncoding.default,
                                               urlParameters: [:])
        case .enterprise(let fetch):
            
            switch  fetch{
            
            case .all:
                
                return .requestPlain
                
            case .by(let name):
                
                return .requestParameters(parameters: ["name" : name], encoding: JSONEncoding.default)
            }
        }
    }
    
    var validate: Bool {
        return false
    }
    
    var headers: [String: String]? {
        
        switch self {
            
        case .login:
            return ["Content-Type" : "application/json"]
            
        case .enterprise:
            return ["Content-Type" : "application/json",
                    "access-token" : User.current?.token ?? "",
                    "client" : User.current?.id ?? "",
                    "uid" : User.current?.email ?? ""]
        }
    }
    
    var validationType: ValidationType {
        return .none
    }
}
