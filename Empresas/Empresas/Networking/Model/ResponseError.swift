//
//  ResponseError.swift
//  Empresas
//
//  Created by Bia on 16/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

struct ResponseError: Codable, LocalizedError {
    
//    let success: Bool
    let errors: [String]
}

struct ResponseSuccess: Codable {
    let success: Bool
}
