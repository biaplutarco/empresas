//
//  UserSession.swift
//  Empresas
//
//  Created by Bia on 16/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

class User {
        
    static var current: User? = User()
    
    var email: String?
    var id: String?
    var token: String?
    
    private static let CURRENT_ID = "CURRENT_SESSION_ID"
    private static let CURRENT_TOKEN = "CURRENT_SESSION_TOKEN"
    private static let CURRENT_EMAIL = "CURRENT_SESSION_EMAIL"
    
    
    private init?() {
        
        let userDefaults = UserDefaults.standard

        let email = userDefaults.value(forKey: User.CURRENT_EMAIL) as? String
        let id = userDefaults.value(forKey: User.CURRENT_ID) as? String
        let token = userDefaults.value(forKey: User.CURRENT_TOKEN) as? String
        
        self.email = email
        self.id = id
        self.token = token
        
    }
    
    static func save(email: String?, id: String?, token: String?) {
        
        let userDefaults = UserDefaults.standard
        
        User.current?.email = email
        User.current?.id = id
        User.current?.token = token
        
        userDefaults.set(token, forKey: User.CURRENT_TOKEN)
        userDefaults.set(id, forKey: User.CURRENT_ID)
        userDefaults.set(email, forKey: User.CURRENT_EMAIL)
    }
}
