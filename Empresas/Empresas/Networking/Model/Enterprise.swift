//
//  Enterprise.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

struct Enterprises: Codable {
    
    let enterprises: [Enterprise]?
    
    enum CodingKeys: String, CodingKey {
        case enterprises
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.enterprises = try values.decode([Enterprise]?.self, forKey: .enterprises)
    }
}

struct Enterprise: Codable {
    
    let id: Int?
    let name: String?
    let photo: String?
    let description: String?
    let country: String?
    let type: EnterpriseType?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_name"
        case photo
        case description
        case country
        case type = "enterprise_type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decode(Int?.self, forKey: .id)
        self.name = try values.decode(String?.self, forKey: .name)
        self.photo = try values.decode(String?.self, forKey: .photo)
        self.description = try values.decode(String?.self, forKey: .description)
        self.country = try values.decode(String?.self, forKey: .country)
        self.type = try values.decode(EnterpriseType?.self, forKey: .type)
    }
}

struct EnterpriseType: Codable {
    
    let id: Int?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_type_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decode(Int?.self, forKey: .id)
        self.name = try values.decode(String?.self, forKey: .name)
    }
}
