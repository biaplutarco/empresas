//
//  UIImage+AppImages.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UIImage {
    
    open class var visibility: UIImage {
        return UIImage(named: "Visibility") ?? UIImage()
    }
    
    open class var cancel: UIImage {
        return UIImage(named: "Cancel") ?? UIImage()
    }
    
    open class var logoIcon: UIImage {
        return UIImage(named: "LogoIcon") ?? UIImage()
    }
}
