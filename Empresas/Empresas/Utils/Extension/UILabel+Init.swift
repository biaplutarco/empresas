//
//  UILabel+Init.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(titleColor: UIColor, andTitle title: String? = nil) {
        
        self.init()
        
        text = title
        textColor = titleColor
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    convenience init(titleColor: UIColor, title: String? = nil, andSize size: CGFloat) {
        
        self.init()
        
        text = title
        textColor = titleColor
        font = UIFont.boldSystemFont(ofSize: size)
        translatesAutoresizingMaskIntoConstraints = false
    }
}
