//
//  UIButton+Init.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UIButton {
    
    convenience init(withColor color: UIColor, titleColor: UIColor, cornerRadius: CGFloat, andTitle title: Text) {
        
        self.init()
        
        setTitle(title.rawValue, for: .normal)
        setTitleColor(titleColor, for: .normal)
        
        layer.cornerRadius = cornerRadius
        backgroundColor = color
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    convenience init(withImage image: UIImage) {
        
        self.init()
        
        setImage(image, for: .normal)
        translatesAutoresizingMaskIntoConstraints = false
    }
}
