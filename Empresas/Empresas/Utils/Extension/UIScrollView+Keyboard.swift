//
//  UIScrollView+Keyboard.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
        
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = convert(keyboardScreenEndFrame, from: window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            contentInset = .zero
        } else {
            contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - safeAreaInsets.bottom, right: 0)
        }

        scrollIndicatorInsets = contentInset
        
//        let selectedRange = selectedRange
//        scrollRangeToVisible(selectedRange)
//
//        
//        
//        guard let userInfo = notification.userInfo,
//            let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
//                return
//        }
//        
//        let adjustmentHeight = (keyboardFrame.cgRectValue.height) * (show ? 1 : -1)
//      
//        contentInset.bottom += adjustmentHeight
//        verticalScrollIndicatorInsets.bottom += adjustmentHeight
    }
}
