//
//  UITextField+Init.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UITextField {
    
    convenience init(withCornerRadius cornerRadius: CGFloat, color: (tint: UIColor, background: UIColor), type: InputType, andRigthButton rigthButton: UIButton? = nil) {
        
        self.init()
        
        tintColor = color.tint
        backgroundColor = color.background
        
        layer.cornerRadius = cornerRadius
        translatesAutoresizingMaskIntoConstraints = false
        
        addPadding(.left(8))
                
        switch type {
            
        case .email:
            
            return
        case .password:
           
            isSecureTextEntry = true

            rightView = rigthButton
            rightViewMode = .unlessEditing
        }
    }
    
    func errorMode(withRightButton rigthButton: UIButton? = nil) {
        
        borderStyle = .line
        layer.borderColor = UIColor.error.cgColor
        layer.borderWidth = 1
        
        rightView = rigthButton
        rightViewMode = .always
    }
}

extension UITextField {
    
    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
    }

    func addPadding(_ padding: PaddingSide) {

        self.layer.masksToBounds = true

        switch padding {

        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always

        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}
