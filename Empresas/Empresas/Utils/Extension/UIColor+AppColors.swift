//
//  UIColor+AppColors.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UIColor {
    
    open class var background: UIColor {
        return UIColor.init(named: "Background") ?? UIColor.black
    }
    
    open class var labelText: UIColor {
        return UIColor.init(named: "LabelText") ?? UIColor.black
    }
    
    open class var action: UIColor {
        return UIColor.init(named: "Action") ?? UIColor.black
    }
    
    open class var error: UIColor {
        return UIColor.init(named: "Error") ?? UIColor.black
    }
    
    open class var top: UIColor {
        return UIColor.init(named: "Top") ?? UIColor.black
    }
    
    open class var middle: UIColor {
        return UIColor.init(named: "Middle") ?? UIColor.black
    }
    
    open class var bottom: UIColor {
        return UIColor.init(named: "Bottom") ?? UIColor.black
    }
}
