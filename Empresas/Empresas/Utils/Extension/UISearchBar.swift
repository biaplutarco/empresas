//
//  UISearchBar.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    convenience init(withColor color: UIColor, corneRadius: CGFloat, andText text: Text) {
        
        self.init()
        
        isTranslucent = false
        
        layer.cornerRadius = corneRadius
        clipsToBounds = true
        
        placeholder = text.rawValue
        
        backgroundImage = UIImage()
        backgroundColor = color
        barTintColor = color
        
        if #available(iOS 13.0, *) {
            searchTextField.backgroundColor = color
        }
    }
}
