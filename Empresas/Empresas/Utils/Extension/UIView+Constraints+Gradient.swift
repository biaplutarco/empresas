//
//  UIView+Constraints.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UIView {
      
    func fulfillSuperview() {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            
            self.topAnchor.constraint(equalTo: superview.topAnchor),
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            
            self.centerYAnchor.constraint(equalTo: superview.centerYAnchor),
            self.centerXAnchor.constraint(equalTo: superview.centerXAnchor)            
        ])
    }
    
    func centeredToSuperview() {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            
            self.centerYAnchor.constraint(equalTo: superview.centerYAnchor),
            self.centerXAnchor.constraint(equalTo: superview.centerXAnchor)
        ])
    }
    
    func headeredToSuperview() {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            
            topAnchor.constraint(equalTo: superview.topAnchor),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor),
        ])
    }
    
    func belowTo(view: UIView) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        guard let superview = self.superview else { return }
        
        NSLayoutConstraint.activate([
            
            topAnchor.constraint(equalTo: view.bottomAnchor, constant: 16),
            heightAnchor.constraint(equalTo: superview.heightAnchor, multiplier: 0.86),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: 16),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -16),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        ])
    }
}

extension UIView {
    
    enum Direction: Int {
        case topToBottom = 0
        case bottomToTop
        case leftToRight
        case rightToLeft
    }
    
    func applyGradient(colors: [UIColor], locations: [NSNumber]? = [0.0, 1.0], direction: Direction = .topToBottom, size: CGSize) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(origin: .zero, size: size)
        gradientLayer.colors = colors
        gradientLayer.locations = locations
        
        switch direction {
            
        case .topToBottom:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            
        case .bottomToTop:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            
        case .leftToRight:
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
        case .rightToLeft:
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
        }
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func doSemiCircle(withSize size: CGSize) {
                
        // ver essa constante do heigth depois
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: size.width/2, y: -size.height * 0.6),
                                      radius: size.width, startAngle: .zero, endAngle: .pi,
                                      clockwise: true)
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        
        layer.mask = circleShape
    }
}

