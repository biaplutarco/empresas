//
//  UITableView+Dequeue+Register.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var identifier: String {
        
        return String(describing: self)
    }
}

extension UITableView {
    
    func dequeueCell<T: UITableViewCell>(of type: T.Type, forIndexPath indexPath: IndexPath) -> T {
    
        guard let cell = self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T
            else {
            
            fatalError("CollectionViewCell wasn't registered")
        }

        return cell
    }
    
    func registerCell<T: UITableViewCell>(of type: T.Type) {
        
        self.register(T.self, forCellReuseIdentifier: T.identifier)
    }
    
}


extension UITableView {
    
    convenience init(withSeparatorStyle separatorStyle: UITableViewCell.SeparatorStyle,
                     backgroundColor: UIColor) {
        
        self.init()
        
        self.separatorStyle = separatorStyle
        self.backgroundColor = backgroundColor
    }
}
