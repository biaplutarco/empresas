//
//  ViewCodeble.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

protocol ViewCodable: class {
    
    func buildViewHierarchy()
    func setupConstraints()
    func setupAdditionalConfiguration()
}

extension ViewCodable {
    
    func setupView() {
        
        buildViewHierarchy()
        setupConstraints()
        setupAdditionalConfiguration()
    }
}

extension ViewCodable where Self: EnterpriseCell {
    
    func setupCell(withViewModel viewModel: EnterpriseCellViewModel) {
        
        self.viewModel = viewModel
        
        setupView()
    }
}

extension ViewCodable where Self: UIViewController {
    
    func setupAdditionalConfiguration() {
        view.backgroundColor = .white
    }
}
