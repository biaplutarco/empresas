//
//  Keyboardeble.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

@objc protocol Keyboardable: class {
    
    @objc func keyboardWillShow(_ notification: Notification)
    @objc func keyboardWillHide(_ notification: Notification)
}

extension Keyboardable {
    
    func addKeyboardObservers() {
        
        NotificationCenter
            .default
            .addObserver( self, selector: #selector(keyboardWillShow(_:)),
                          name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter
            .default
            .addObserver(self, selector: #selector(keyboardWillHide(_:)),
                         name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardObservers() {
        
        NotificationCenter
            .default
            .removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter
            .default
            .removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
