//
//  Searchable.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

protocol Searchable {
    
    var isFiltering: Bool { get set }
    var searchText: String { get set }
    var data: [Enterprise] { get set }
}

extension Searchable {
    
    func allEnterprises(completionHandler: @escaping ([Enterprise]?, Error?)-> Void) {
        
        let provider = BackpackerProvider<Enterprises, SessionAPI>()

        provider.request(.enterprise(.all)).done { response in

            completionHandler(response.enterprises, nil)
        }.catch { error in
            
            completionHandler(nil, error)
        }
    }
    
    func search(byName name: String, completionHandler: @escaping (Enterprise?, Error?)-> Void) {
        
        let provider = BackpackerProvider<Enterprise, SessionAPI>()

        provider.request(.enterprise(.by(name: name))).done { response in

            completionHandler(response, nil)
        }.catch { error in
            
            completionHandler(nil, error)
        }
    }
}
