//
//  Loginable.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

protocol Loginable { }

extension Loginable {
    
    func doLogin(withUserCredentials userCredentials: [String : Any]) {
        
        let provider = BackpackerProvider<ResponseSuccess, SessionAPI>()

        provider.request(.login(bodyParameters: userCredentials)).done { response in

            NotificationCenter.default.post(name: Notification.Name("userDidLogin"), object: nil)
            
        }.catch { error in
            
            NotificationCenter.default.post(name: NSNotification.Name("LoginError"), object: nil)
        }

    }
}

@objc protocol LoginSuccessful: class {
    
    @objc func userDidLogin()
}

extension LoginSuccessful {
    
    func addLoginObserver() {
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(userDidLogin),
                         name: NSNotification.Name("userDidLogin"), object: nil)
    }
    
    func removeLoginObserver() {
        NotificationCenter
            .default
            .removeObserver(self, name: NSNotification.Name("userDidLogin"), object: nil)
    }
}
