//
//  Text.swift
//  Empresas
//
//  Created by Bia on 17/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

enum Text: String {
    
    case siginin = "ENTRAR"
    case welcome = "Seja bem vindo ao empresas!"
    case email = "Email"
    case password = "Senha"
    case credentialsError = "Credenciais Incorretas"
    case searchPlaceholder = "Pesquise por empresas"
    case searchResults = "resultados encontrados"
    case notFound = "Nenhum resultado encontrado"
}
