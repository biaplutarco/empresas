//
//  InputType.swift
//  Empresas
//
//  Created by Bia on 19/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import Foundation

enum InputType: String {
    case email = "Email"
    case password = "Senha"
}
