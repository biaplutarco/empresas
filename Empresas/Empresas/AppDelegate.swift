//
//  AppDelegate.swift
//  Empresas
//
//  Created by Bia on 13/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let viewController = SearchViewController()
        
        let window = UIApplication.shared.delegate?.window!
        window!.rootViewController = viewController
        window!.makeKeyAndVisible()
        
        if User.current?.id == nil {
            
            let loginViewController = LoginViewController()
            loginViewController.modalPresentationStyle = .fullScreen

            viewController.present(loginViewController, animated: true, completion: nil)
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        return UISceneConfiguration(name: "Default Configuration",
                                    sessionRole: connectingSceneSession.role)
    }
}
