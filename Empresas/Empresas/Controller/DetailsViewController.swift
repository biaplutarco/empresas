//
//  EnterpriseViewController.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    private var viewModel: DetailsViewModel
    
    private lazy var detailsView: DetailsView = {
        let detailsView = DetailsView()
        
        return detailsView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        detailsView.viewModel = viewModel
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DetailsViewController: ViewCodable {
    
    func buildViewHierarchy() {
        view.addSubview(detailsView)
    }
    
    func setupConstraints() {
        detailsView.fulfillSuperview()
    }
}
