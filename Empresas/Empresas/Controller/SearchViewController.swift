//
//  SearchViewController.swift
//  Empresas
//
//  Created by Bia on 18/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, LoginSuccessful {
    
    private lazy var searchView: SearchView = {
        let searchView = SearchView(viewModel: SearchViewModel())
        searchView.delegate = self
        
        return searchView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        addLoginObserver()
    }
    
    deinit {
        removeLoginObserver()
    }
    
    func userDidLogin() {
        searchView.reloadData()
    }
}

extension SearchViewController: ViewCodable {
    
    func buildViewHierarchy() {
        view.addSubview(searchView)
    }
    
    func setupConstraints() {
        searchView.fulfillSuperview()
    }
}

extension SearchViewController: SearchViewDelegate {
    
    func present(_ viewcontroller: DetailsViewController) {
        self.present(viewcontroller, animated: true, completion: nil)
    }
}
