//
//  ViewController.swift
//  Empresas
//
//  Created by Bia on 13/04/20.
//  Copyright © 2020 akhaten. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginSuccessful {
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        
        return scrollView
    }()
    
    private lazy var loginView: LoginView = {
        let size = CGSize(width: view.bounds.width, height: view.bounds.height * 0.3)
        let loginView = LoginView(withSemiCircleOf: size)
        
        return loginView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        addKeyboardObservers()
        addLoginObserver()
    }
    
    deinit {
        removeKeyboardObservers()
        removeLoginObserver()
    }
    
    @objc func userDidLogin() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LoginViewController: ViewCodable {
    
    func buildViewHierarchy() {

        view.addSubview(scrollView)
        scrollView.addSubview(loginView)
    }
    
    func setupConstraints() {
        
        scrollView.fulfillSuperview()
        loginView.fulfillSuperview()
    }
}

extension LoginViewController: Keyboardable {
    
    @objc func keyboardWillShow(_ notification: Notification) {
        scrollView.adjustInsetForKeyboardShow(true, notification: notification)
    }
       
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.adjustInsetForKeyboardShow(false, notification: notification)
    }
}
